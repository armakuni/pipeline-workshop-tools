# Pipeline Workshop Tools

This repository contains the in class artifacts that assist in learning how to do pipelines in a "DevOps", "CD" style.

## Contents

1. [Writer](#writer)

## Writer

Write "recipes" that need to be further linted by other steps in a pipeline.

### Downloading

See [Downloads](https://bitbucket.org/armakuni/pipeline-workshop-tools/downloads/)

### Building

To build this application run 

```shell
make build
```

and the application will be in the bin directory.

To compile cross platform set the `GOOS` and `GOARCH` environment variables from. Values can by running

```
go tool dist list
```

and take the format `GOOS/GOARCH` for example `windows/amd64`. Unprovided values default to your systems architecture or os.
