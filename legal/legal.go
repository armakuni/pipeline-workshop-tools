package main

import (
	"fmt"
	"io/ioutil"
	"math/rand"
	"os"
	"time"
)

func main() {
	_, err := ioutil.ReadAll(os.Stdin)

	if err != nil {
		panic(err)
	}

	rand.Seed(time.Now().UTC().UnixNano())
	time.Sleep(time.Duration(rand.Intn(61)) * time.Second)

	if rand.Intn(2) == 0 {
		fmt.Fprintf(os.Stderr, "Legal has rejected your recipe!")
		os.Exit(1)
	}

	_, _ = fmt.Println("Congratulations, Legal has approved your recipe for publication!")

	os.Exit(0)
}
