#!/usr/bin/env bash

set -euo pipefail
SCRIPT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null && pwd)"
ROOT_DIR="$SCRIPT_DIR/.."

for I in {1..100}; do
	if make -C "${ROOT_DIR}" run; then
    echo -e "Run $I: Pass!"
		exit 0
	fi

	echo -e "Run $I: Fail!"
done

exit 1
