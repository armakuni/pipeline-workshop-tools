#!/usr/bin/env bash

set -xeuo pipefail
SCRIPT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null && pwd)"
ROOT_DIR="$SCRIPT_DIR/.."

declare -A ENVIRONMENT_AN_FILENAME
ENVIRONMENT_AN_FILENAME=(["windows"]="windows.exe" ["darwin"]="darwin" ["linux"]="linux")
TEMP_DIR="$(mktemp -d)"

for GOOS in "${!ENVIRONMENT_AN_FILENAME[@]}"; do
	export GOOS
	make -C "$ROOT_DIR" clean build

	for FILE in "$ROOT_DIR"/bin/*; do
		mv "$FILE" "$TEMP_DIR/$(basename "$FILE")-${ENVIRONMENT_AN_FILENAME[$GOOS]}"
	done
done

cp "$TEMP_DIR"/* "$ROOT_DIR/bin/"
