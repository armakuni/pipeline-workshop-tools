package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"strings"
)

func main() {
	bytes, err := ioutil.ReadAll(os.Stdin)

	if err != nil {
		panic(err)
	}

	fileContents := string(bytes)
	fmt.Println(fileContents)

	if hasSpellingMistakes(fileContents) {
		_, _ = fmt.Fprintf(os.Stderr, "Spelling mistake detected!")
		os.Exit(1)
	}

	_, _ = fmt.Println("No spelling mistakes!")
	os.Exit(0)
}

func hasSpellingMistakes(original string) bool {
	if containsLetter(original, "a") || containsLetter(original, "A") {
		return false
	}

	return true
}

func containsLetter(haystack string, needle string) bool {
	return strings.Index(haystack, needle) != -1
}
