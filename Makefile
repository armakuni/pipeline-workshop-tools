.DEFAULT_GOAL := show-help
THIS_FILE := $(lastword $(MAKEFILE_LIST))
ROOT_DIR:=$(shell dirname $(realpath $(THIS_FILE)))

.PHONY: show-help
# See <https://gist.github.com/klmr/575726c7e05d8780505a> for explanation.
## This help screen
show-help:
	@echo "$$(tput bold)Available rules:$$(tput sgr0)";echo;sed -ne"/^## /{h;s/.*//;:d" -e"H;n;s/^## //;td" -e"s/:.*//;G;s/\\n## /---/;s/\\n/ /g;p;}" ${MAKEFILE_LIST}|LC_ALL='C' sort -f|awk -F --- -v n=$$(tput cols) -v i=29 -v a="$$(tput setaf 6)" -v z="$$(tput sgr0)" '{printf"%s%*s%s ",a,-i,$$1,z;m=split($$2,w," ");l=n-i;for(j=1;j<=m;j++){l-=length(w[j])+1;if(l<= 0){l=n-i-length(w[j])-1;printf"\n%*s ",-i," ";}printf"%s ",w[j];}printf"\n";}'

.PHONY: build
## Build all binaries
build:
	make -C writer build
	make -C spellchecker build
	make -C formatter build
	make -C legal build

.PHONY: cross-platform-build
## Build for linux, windows and mac
cross-platform-build:
	${ROOT_DIR}/ci/build-for-all-os.sh

.PHONY: release-bin-dir
## Upload all binaries in in bin directory, requires BB_AUTH_STRING to be set
release-bin-dir:
	${ROOT_DIR}/ci/release-bin-dir.sh

.PHONY: test
## Run all tests
test: test-unit test-e2e

.PHONY: test-unit
## Test all binaries unit tests
test-unit:
	make -C writer test
	make -C spellchecker test
	make -C formatter test
	make -C legal test

.PHONY: clean
## Remove build binaries
clean:
	make -C writer clean
	make -C spellchecker clean
	make -C formatter clean
	make -C legal clean

.PHONY: run
## Build an example pipeline and run it
run: build
	./ci/run.sh

.PHONY: test-e2e
## Build an example pipeline and run it
test-e2e: build
	./ci/e2e.sh
