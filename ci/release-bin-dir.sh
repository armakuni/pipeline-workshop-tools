#!/usr/bin/env bash

set -euo pipefail

SCRIPT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null && pwd)"
ROOT_DIR="$SCRIPT_DIR/.."
BITBUCKET_REPO_OWNER="${BITBUCKET_REPO_OWNER:-armakuni}"
BITBUCKET_REPO_SLUG="${BITBUCKET_REPO_SLUG:-pipeline-workshop-tools}"

for FILE in "$ROOT_DIR"/bin/*; do
	curl -X POST \
		"https://${BB_AUTH_STRING}@api.bitbucket.org/2.0/repositories/${BITBUCKET_REPO_OWNER}/${BITBUCKET_REPO_SLUG}/downloads" \
		--form files=@"$FILE"
done
