package main

import (
	"fmt"
	"math/rand"
	"strings"
	"time"

	"github.com/bxcodec/faker"
)

type ingredient struct {
	Ingredient string `faker:"word"`
}

type article struct {
	Introduction string `faker:"paragraph"`
	Body1        string `faker:"paragraph"`
	Body2        string `faker:"paragraph"`
	Body3        string `faker:"paragraph"`
	Conclusion   string `faker:"paragraph"`
}

func main() {
	recipe := generateRecipe()

	rand.Seed(time.Now().UTC().UnixNano())

	if rand.Intn(2) == 0 {
		recipe = introduceSpellingMistakes(recipe)
	}

	fmt.Print(recipe)
}

func generateRecipe() string {
	ingredients := make([]ingredient, 5)
	for index := range ingredients {
		err := faker.FakeData(&ingredients[index])

		if err != nil {
			fmt.Println(err)
		}
	}

	a := article{}
	err := faker.FakeData(&a)

	if err != nil {
		fmt.Println(err)
	}

	return fmt.Sprintf("%dg %s\n%dg %s\n%dg %s\n%dg %s\n%dg %s\n\n%s\n\n%s\n\n%s\n\n%s\n\n%s",
		rand.Intn(500),
		ingredients[0].Ingredient,
		rand.Intn(500),
		ingredients[1].Ingredient,
		rand.Intn(500),
		ingredients[2].Ingredient,
		rand.Intn(500),
		ingredients[3].Ingredient,
		rand.Intn(500),
		ingredients[4].Ingredient,
		a.Introduction,
		a.Body1,
		a.Body2,
		a.Body3,
		a.Conclusion)
}

func introduceSpellingMistakes(original string) string {
	modified := strings.ReplaceAll(original, "a", "")
	return strings.ReplaceAll(modified, "A", "")
}
