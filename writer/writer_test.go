package main

import (
	"github.com/stretchr/testify/assert"
	"io/ioutil"
	"os"
	"regexp"
	"strings"
	"testing"
)

func TestOutputsANoneZeroLengthString(t *testing.T) {
	stdout := runMain()
	assert.False(t, len(stdout) <= 0, "main() output \"%s\"; want content longer than 0", stdout)
}

func TestThereAre5Paragraphs(t *testing.T) {
	stdout := runMain()
	assert.False(t, len(strings.Split(stdout, "\n\n")) == 5, "main() output \"%s\"; should be 5 paragraphs", stdout)
}

func TestThereIsAtLeastOneIngredient(t *testing.T) {
	recipe := generateRecipe()
	actual, err := regexp.MatchString("\\d+\\w \\w+", recipe)

	assert.True(t, actual)
	assert.Nil(t, err)
}

func TestThereShouldBeNoLowercaseAs(t *testing.T) {
	text := "Pariatur voluptate magna adipisicing et sunt sint anim veniam ullamco. Commodo ea id reprehenderit enim veniam do. Eu adipisicing consequat aute aliqua irure exercitation. Sint aliquip elit veniam eiusmod elit adipisicing aliqua. Laborum dolore proident et deserunt voluptate ullamco laborum aute nulla dolor id in irure. Anim sint ea anim nisi dolore officia minim non minim cupidatat dolore. Lorem nostrud magna culpa aute dolore laborum irure minim do sit."
	text = introduceSpellingMistakes(text)

	assert.False(t, strings.Index(text, "a") != -1, "main() output \"%s\"; should contain no letter a's", text)
}
func TestThereShouldBeNoUppercaseAs(t *testing.T) {
	text := "Pariatur voluptate magna adipisicing et sunt sint anim veniam ullamco. Commodo ea id reprehenderit enim veniam do. Eu adipisicing consequat aute aliqua irure exercitation. Sint aliquip elit veniam eiusmod elit adipisicing aliqua. Laborum dolore proident et deserunt voluptate ullamco laborum aute nulla dolor id in irure. Anim sint ea anim nisi dolore officia minim non minim cupidatat dolore. Lorem nostrud magna culpa aute dolore laborum irure minim do sit."
	text = introduceSpellingMistakes(text)

	assert.False(t, strings.Index(text, "A") != -1, "main() output \"%s\"; should contain no letter A's", text)
}

func runMain() string {
	r, w, _ := os.Pipe()
	tmp := os.Stdout
	defer func() {
		os.Stdout = tmp
	}()
	os.Stdout = w
	go func() {
		main()
		w.Close()
	}()
	stdout, _ := ioutil.ReadAll(r)
	return string(stdout)
}
