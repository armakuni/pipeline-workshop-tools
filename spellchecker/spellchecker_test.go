package main

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestNoLetterAIndicatesSpellingMistake(t *testing.T) {
	assert.Equal(t, true, hasSpellingMistakes("No letter"))
}

func TestThereIsNoSpellingMistakeIfThereIsACapitalA(t *testing.T) {
	assert.Equal(t, false, hasSpellingMistakes("A"))
}

func TestThereIsNoSpellingMistakeIfThereIsALowerCaseA(t *testing.T) {
	assert.Equal(t, false, hasSpellingMistakes("aa"))
}

func TestNoSpellingMistakesIfLowercaseAAndUppercaseA(t *testing.T) {
	assert.Equal(t, false, hasSpellingMistakes("Aa"))
}
