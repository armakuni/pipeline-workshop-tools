#!/usr/bin/env bash

set -xeuo pipefail
TEMP_FILE="$(mktemp)"

print_recipe() {
  echo "--- $TEMP_FILE ---"
  echo
  cat <"$TEMP_FILE"
  echo
  echo "--- $TEMP_FILE ---"
}

trap 'print_recipe' ERR

make -C writer run >"$TEMP_FILE"
make -C spellchecker run <"$TEMP_FILE"
make -C formatter run <"$TEMP_FILE" | make -C legal run
