package main

import (
	"fmt"
	"io/ioutil"
	"os"
)

const htmlTemplate = `<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>FamilyDough Recipe</title>
</head>
<body>
	<p>
		%s
	</p>
</body>
</html>
`

func main() {
	bytes, err := ioutil.ReadAll(os.Stdin)

	if err != nil {
		panic(err)
	}

	fmt.Printf(htmlTemplate, string(bytes))
}
