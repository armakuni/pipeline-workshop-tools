module writer

go 1.13

require (
	github.com/bxcodec/faker v2.0.1+incompatible
	github.com/stretchr/testify v1.4.0
)
